package controller;

import utility.*;

public class PayrollProgram {	

	public static void main(String[] args) {
		
		// input variables
		int dependents = 0;
		double hoursWorked = 0;
		String exit;
		
		// output variables
		double grossPay, socialSecurityTax, HDMF, professionalIncomeTax,
				healthInsurance, takeHomePay;
		
		while (true) { // infinite loop; break to exit the program
			
			// accept input
			hoursWorked = KBInput.readInt("\nEnter the number of hours worked this week: ");
			while (hoursWorked <= 0) { // error input validation for hours worked (must not be less than or equal to 0)
				System.out.println("Invalid Input! Hours worked must be greater than 0!");
				hoursWorked = KBInput.readInt("Enter the number of hours worked this week: ");
			}
			
			dependents = KBInput.readInt("Enter the number of dependents: ");
			while (dependents < 0) { // error input validation for dependents (must not be less than 0)
				System.out.println("Invalid Input! Dependents must not be a negative value!");
				dependents = KBInput.readInt("Enter the number of dependents: ");
			}			
			
			// calculate input
			grossPay 				= payroll.grossPayCompute(hoursWorked);
			socialSecurityTax 		= payroll.socialSecurityTaxCompute(grossPay);
			HDMF 					= payroll.HDMFCompute(grossPay);
			professionalIncomeTax 	= payroll.professionalIncomeTaxCompute(grossPay);
			healthInsurance 		= payroll.healthInsuranceCompute(dependents);
			takeHomePay 			= payroll.netTakeHomePayCompute(grossPay, socialSecurityTax, HDMF, professionalIncomeTax, healthInsurance);
			
			// print output
			System.out.println("\n============== CALCULATED PRICES =================");
			
			System.out.println("Gross Pay\t\t\t: P" + 
					String.format("%14.2f", grossPay));
			
			System.out.println("Social Security Tax\t\t: P" + 
					String.format("(%13.2f)", socialSecurityTax));
			
			System.out.println("HDMF\t\t\t\t: P" + 
					String.format("(%13.2f)", HDMF));
			
			System.out.println("Professional Income Tax\t\t: P" + 
					String.format("(%13.2f)", professionalIncomeTax));
			
			System.out.println("Health Insurance\t\t: P" + 
					String.format("(%13.2f)", healthInsurance)); 
			
			System.out.println("--------------------------------------------------");
			
			System.out.println("Net Take Home Pay for the Week\t: P" + 
					String.format("%14.2f", takeHomePay)); 
			
			System.out.println("==================================================");
			
			
			// exit program
			exit = KBInput.readString("\nDo you want to continue? [Y/n] ");
			if(exit.equalsIgnoreCase("n")) 
				break;
			
		}
		
		// exit message
		System.out.println("\n\nThank you for using this simple Payroll Program!");
		
		
	}

}