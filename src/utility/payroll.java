package utility;

public class payroll {
	
	public static double grossPayCompute(double hoursWorked) {
		
		double grossPay = 0;
		
		if(hoursWorked <= 40) {
			grossPay = hoursWorked * 500;
		}
		else {
			grossPay = 20000 + ((hoursWorked - 40) * (1.5 * 500));
		}
		
		return grossPay;
	}

	public static double socialSecurityTaxCompute(double grossPay) {
		return grossPay * 0.015;
	}
	
	public static double HDMFCompute(double grossPay) {
		return grossPay * 0.03;
	}
	
	public static double professionalIncomeTaxCompute(double grossPay) {
		return grossPay * 0.1;
	}
	
	public static double healthInsuranceCompute(int dependents) {
		
		double healthInsurance = 0;
		
		if(dependents >= 3) {
			healthInsurance += 300;
		}
		
		return healthInsurance;
	}
	
	public static double netTakeHomePayCompute(double grossPay, double socialSecurityTax, double HDMF, double professionalIncomeTax, double healthInsurance) {
		return grossPay - socialSecurityTax - HDMF - professionalIncomeTax - healthInsurance;
	}
	
	
}
